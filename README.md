# Generate sitemaps with ease

[![Latest Version on Packagist](https://img.shields.io/packagist/v/proscom/laravel-sitemap.svg?style=flat-square)](https://packagist.org/packages/proscom/laravel-sitemap)
[![Software License](https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square)](LICENSE.md)
[![Total Downloads](https://img.shields.io/packagist/dt/proscom/laravel-sitemap.svg?style=flat-square)](https://packagist.org/packages/proscom/laravel-sitemap)

This package is a fork of https://github.com/spatie/laravel-sitemap.
You can view the original readme there.

It includes ability to render image tags in the sitemap, like this:

```php
$url = Url::create('/');
$url->addChild(Image::create(
    '/image.png', // url
    'Caption', // caption
    'Title' // title
));
```


